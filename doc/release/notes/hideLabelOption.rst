Added a Mechanism to hide labels for pqSMTKAttributeItemWidget
--------------------------------------------------------------

You can now suppress the item's label from being displayed by simply setting the label value to be " " (note a single blank and not the empty string).  As long as the item is not optional, its label will not be displayed.
