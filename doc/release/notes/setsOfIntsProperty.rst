Expanded SMTK Properties to include sets of ints
-------------------------------------------------

You can now create a property that is represented as a set of integers
