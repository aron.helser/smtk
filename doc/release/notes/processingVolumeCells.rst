Added the Ability to Render Geometry with Solid Cells
------------------------------------------------------

* vtkResourceMultiBlockSource will now put Components with Solid Cells under its component Component Block
* vtkApplyTransfors will now also do a surface extraction for components with solid cells
